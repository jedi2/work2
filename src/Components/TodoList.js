import React, { useState } from 'react';

function TodoList(props) {

  const [taskEdit, setTaskEdit] = useState({});
  const editMode = (task) => {
    setTaskEdit(task);
  };
  const onEditTaskChage = (e) => {
    setTaskEdit({ ...taskEdit, name: e.taget.value });
  };

  const taskSave = () => {
    props.onSaveTask(taskEdit);
    setTaskEdit('');
  };

  return (
    <div>
      {props.todos.map(el => <li key={el.id}>
        {el.done ? '✅' : '❌'}
        {
          taskEdit.id === el.id
            ?
            <>
              <input type="text" value={taskEdit.name} onChange={onEditTaskChage} />
              <button onClick={taskSave} disabled={!taskEdit.name === ''}>Save</button>
            </>
            :
            <span onClick={() => editMode(el)}>{el.name}</span>
        }
        <button onClick={() => props.onDeleteTask(el.id)}>Delete</button>
        <button onClick={() => props.onDoneTaskToggle(el.id)}>{el.done ? 'Undone' : 'Done'}</button>
      </li>)}
    </div>
  );
}

export default TodoList;
