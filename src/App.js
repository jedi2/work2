import React, { useState } from 'react';
import TodoList from './Components/TodoList';
import TodoCreateForm from './Components/TodoCreateForm';

function App() {

  const initialTodos = [
    { id: 1, name: 'Wake up', done: false },
    { id: 2, name: 'Check Phone', done: false },
    { id: 3, name: ' Text', done: true },
  ];
  const [todos, setTodos] = useState(initialTodos);

  const onCreateTask = (task) => {
    const updatedTodos = [...todos]
    updatedTodos.push({ id: Math.random(), name: task, done: false });
    setTodos(updatedTodos);
  };
  const onDeleteTask = (id) => {
    const updatedTodos = todos.filter(el => el.id !== id);
    setTodos(updatedTodos);
  };
  const onDoneTaskToggle = (id) => {
    const updatedTodos = todos.map(el => {
      return el.id === id ? { ...el, done: !el.done } : el
    });
    setTodos(updatedTodos);
  };
  const onSaveTask = (task) => {
    const updatedTodos = todos.map(el => {
      return el.id === task.id ? { ...el, name: task.name } : el
    });
    setTodos(updatedTodos);
  };



  return (
    <div>
      <h1>To do list</h1>
      <h2>Total Todos : {todos.length}</h2>
      <h3>Undone : {todos.filter(el => !el.done).length}</h3>
      <h3>Done : {todos.filter(el => el.done).length}</h3>
      <TodoCreateForm onCreateTask={onCreateTask} />
      <hr />
      <h3>To do:</h3>

      <TodoList todos={todos}
        onDeleteTask={onDeleteTask}
        onDoneTaskToggle={onDoneTaskToggle}
        onSaveTask={onSaveTask}
      />
    </div>
  );
}

export default App;
